from __future__ import absolute_import, unicode_literals

from celery import shared_task
from .monthly_leave_grant_update import monthly_update
# from django.core.management import call_command


# @shared_task()
# def monthly_leave_grant_update():
#   call_command("payment_due")

@shared_task()
def monthly_leave_grant_update_task():
  monthly_update()

@shared_task
def add(x, y):
  return x + y