from django.urls import path
from hr_leave_grant_app.views import (  BeneficiaryListCreateAPIView,
                                        BeneficiaryDetailAPiView,
                                        DependentCreateAPIView,
                                        DependentDetailAPIView,

                                        BeneficiaryListView,
                                        BeneficiaryDetailView,
                                        BeneficiaryPaymentsDetailView,
                                        ConfirmPaymentView,
                                        CreateBeneficiaryView,
                                        CreateDependentView,
                                        PendingPaymentView,
                                        UpdateBeneficiaryView,
                                        UpdateDependentView,
                                        DependentListView,
                                        DependentOver18ListView,
                                        TransactionHistoryView)

urlpatterns = [
    path('api/beneficiaries/', 
        BeneficiaryListCreateAPIView.as_view(),
        name='beneficiary-list'),

    path('api/beneficiary/<int:pk>/',
        BeneficiaryDetailAPiView.as_view(),
        name='benficiary-detail'),    
    

    path('api/beneficiary/<int:beneficiary_pk>/dependent/',
        DependentCreateAPIView.as_view(),
        name='dependent-create'),

    
    path('api/dependent/<int:pk>/',
        DependentDetailAPIView.as_view(),
        name='dependent-detail'),

    path('',
        BeneficiaryListView.as_view(), 
        name='beneficiary'),

    path('hr/leave-grant/beneficiary/new/',
        CreateBeneficiaryView.as_view(), 
        name='beneficiary-new'),

    path('hr/leave-grant/beneficiary/update/<int:pk>',
        UpdateBeneficiaryView.as_view(), 
        name='beneficiary-update'),   

    path('hr/leave-grant/beneficiary/<int:pk>/',
        BeneficiaryDetailView.as_view(), 
        name='beneficiary-detail'),

     path('hr/leave-grant/beneficiary/<int:pk>/payment/',
        BeneficiaryPaymentsDetailView.as_view(), 
        name='beneficiary-payment'),

    path('hr/leave-grant/beneficiary/<int:pk>/payment/confirm/',
        ConfirmPaymentView.as_view(), 
        name='confirm-payment'),

    # path('hr/leave-grant/dependent/new/',
    #     CreateDependentView.as_view(), 
    #     name='dependent-new'),

    path('hr/leave-grant/beneficiary/<int:pk>/dependent/new/',
        CreateDependentView.as_view(), 
        name='dependent-new'),

    path('hr/leave-grant/dependent/<int:pk>/',
        UpdateDependentView.as_view(), 
        name='dependent-detail'),

    path('hr/leave-grant/dependent/all/',
        DependentListView.as_view(), 
        name='dependent-all'),

    path('hr/leave-grant/dependent/over18/',
        DependentOver18ListView.as_view(), 
        name='dependent-over18'),

    path('hr/leave-grant/payment/pending/',
        PendingPaymentView.as_view(), 
        name='pending-payment'),

    path('hr/leave-grant/payment/transaction-history/',
        TransactionHistoryView.as_view(), 
        name='transaction-history'),

]
