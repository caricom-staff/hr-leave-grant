<script>
    /*
        This script checks to see if the active checkbox is checked
        and disables the further education checkbox if it is
    
    */

let active_checkbox = document.getElementById('id_active')
let fe_checkbox = document.getElementById('id_further_education')

function further_education_check(){
    if ((active_checkbox).checked ){        
        fe_checkbox.disabled = false; 
    }else {
        
        fe_checkbox.disabled = true;
        fe_checkbox.checked = false;
    }
}

document.addEventListener("DOMContentLoaded", function(event){   
    further_education_check()
    
})

active_checkbox.addEventListener('click', () => {
    further_education_check()
})

</script>