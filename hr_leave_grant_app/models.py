from django.db import models
from django.urls import reverse
import datetime

class Leave_Grant_Level(models.Model):
    'The leave grant amount for each year and level'    
    level = models.CharField(max_length=20)
    leave_grant_percentage = models.PositiveIntegerField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        ordering = ['leave_grant_percentage', ]
        verbose_name_plural = 'Leave Grant Levels'
    
    def __str__(self):
        return f'{self.level}'


class Leave_Grant_Year_Value(models.Model):
    'Stores the value of the leave grant for each year'
    year = models.DateField(auto_now_add=False)
    value = models.DecimalField(max_digits=6,decimal_places=2)

    class Meta:
        ordering = ['-year']
        verbose_name_plural = 'Leave Grant Year Values'

    def __str__(self):
        return f'{self.year} - ${self.value}'
    


class Beneficiary(models.Model):
    'Beneficiary Bio Data'

    employee_id = models.CharField(max_length=5, unique=True, verbose_name="Employee ID")
    first_name = models.CharField(max_length=30, verbose_name="First Name")
    last_name = models.CharField(max_length=30, verbose_name="last Name")
    eligibility_date = models.DateField(auto_now_add=False, verbose_name="Eligibility Date")
    active = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    leave_grant_level = models.ForeignKey(Leave_Grant_Level, on_delete=models.PROTECT, related_name='leave_grant_levels', verbose_name="Leave Grant Level")
    job_title = models.CharField(max_length=100, verbose_name="Job Title")
    card = models.FileField(upload_to='leave_grant_cards/', blank=True, null=True, default=None)
    
    class Meta:
            verbose_name_plural = 'Beneficiaries'
            ordering = ['first_name', 'last_name']
   
    
    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    
    def get_absolute_url(self):
        return reverse('beneficiary-detail', args=[str(self.id)])

   


class Dependent(models.Model):
    'Dependents Bio Data'
    DEPENDENT_TYPE_CHOICES = (
                    ('Daughter', 'Daughter'),
                    ('Father', 'Father'),
                    ('Husband', 'Husband'),
                    ('Mother', 'Mother'),
                    ('Son', 'Son'),
                    ('Wife', 'Wife'),
    )

    beneficiary = models.ForeignKey(Beneficiary, on_delete=models.CASCADE, related_name='dependent')    
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    dependent_type = models.CharField(max_length=20, choices=DEPENDENT_TYPE_CHOICES)
    date_of_birth = models.DateField(auto_now_add=False)
    active = models.BooleanField()
    further_education = models.BooleanField(default=False)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:        
        ordering = ['first_name', 'last_name']


    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def over_18(self):
        'Returns True if the dependents age > 18'
        return  int(((datetime.date.today() - self.date_of_birth).days) / 365.25) >= 18 


    # def get_absolute_url(self):
    #     return reverse('beneficiary-detail', args=[str(self.id)])


    


class Leave_Grant_Detail(models.Model):
    'Keeps track of leave grant payments for beneficiaries and dependents'
    beneficiary = models.ForeignKey(Beneficiary, on_delete=models.CASCADE, related_name='leave_grant_details')       
    payment_date = models.DateField(auto_now_add=False,blank=True, null=True)
    amount = models.DecimalField(max_digits=7,decimal_places=2)
    remarks = models.TextField(null=True,blank=True)
    transaction_year = models.DateField(blank=True, null=True) #Payment for specific year
    recorded_by = models.CharField(max_length=50, verbose_name="Recorded By", blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)


    class Meta:
        ordering = ['beneficiary', '-payment_date', ]
        verbose_name_plural = 'Leave Grant Details'

    def __str__(self):
        return f'{self.payment_date} - {self.amount} - {self.remarks}'