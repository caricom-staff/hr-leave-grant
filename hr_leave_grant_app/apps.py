from django.apps import AppConfig


class HrLeaveGrantAppConfig(AppConfig):
    name = 'hr_leave_grant_app'
