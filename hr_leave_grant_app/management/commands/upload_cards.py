from django.core.files import File
from django.core.management.base import BaseCommand
from hr_leave_grant_app.models import Beneficiary

from os import listdir,getcwd, path

class Command(BaseCommand):
  help = 'Imports Leave Grant Cards'

  def handle(self, *args, **kwargs):
    
    cards_path = getcwd()    
    cards_path= getcwd() + '/hr_leave_grant_app/management/commands/cards'    
    cards = listdir(cards_path)

    for card in cards:
      employee= card.split('_')
      beneficiary = Beneficiary.objects.filter(employee_id=employee[0])
      if not beneficiary:
        print(employee)
      else: 
        card = 'leave_grant_cards/' + card
        beneficiary = Beneficiary.objects.filter(employee_id=employee[0]).update(card=card)

    # beneficiaries = Beneficiary.objects.filter(eligibility_date__month__gte='01', eligibility_date__month__lt='07', active=True)
    # for ben in beneficiaries:
    #   print(ben)  
    # print(beneficiaries.count()) 
        
    # MyModel.objects.filter(pk=some_value).update(field1='some value')