from django.core.management.base import BaseCommand
from hr_leave_grant_app.models import Dependent, Beneficiary

import csv

class Command(BaseCommand):
    help = 'Imports Dependents Records'
   

    def add_arguments(self, parser):
        parser.add_argument('file_name', help='File name')

   

       


    def handle(self, *args, **kwargs):
        file_name = kwargs['file_name']
       
        
        with open(file_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            
            for row in csv_reader:
                try:
                    beneficiary = Beneficiary.objects.get(employee_id=row[0])                    
                    print(f'Employee Found -{row[1]} {row[2]}')
                    
                    try:
                        if row[8] == 'Yes':
                            active = True
                        else:
                            active = False 

                        if row[9] == 'Yes':
                            further_education = True
                        else:
                            further_education = False


                        Dependent.objects.create(beneficiary = beneficiary,
                                                first_name = row[6],
                                                last_name = row[5],
                                                dependent_type = row[3],
                                                date_of_birth = row[7],
                                                active = active,
                                                further_education = further_education
                        )
                        print(f'Added Dependent - {row[6]} {row[5]}')
                    except Exception as e:
                        print(f'Unable to add Depedent {row[6]} {row[5]} {e}')


                except Exception as e:
                    print(f'Employee NOT FOUND {row[1]} {row[2]} ...{e}')
                
   





