from django.core.management.base import BaseCommand
from hr_leave_grant_app.models import Beneficiary, Dependent, Leave_Grant_Detail, Leave_Grant_Level

import csv

class Command(BaseCommand):
    help = 'Imports Beneficiary Records'
    count = 0
    error_records = []

    def add_arguments(self, parser):
        parser.add_argument('file_name', help='File name')

    def add_payment(self, date,amount, who, remarks=None):
        payments = {}
        payments['date'] = date
        payments['amount'] = amount
        if remarks is not None:
            payments['who'] = who + ' - '+remarks
        else:
            payments['who'] = who
    
        return payments

    def add_bio(self,id,fname,lname,edate,job, level,active):
        employee = {}
        employee['employee_id']= id
        employee['first_name']= fname
        employee['last_name']= lname
        employee['eligibility_date']= edate
        employee['job_title']= job
        employee['level'] = level
        if active == '':
            employee['active'] = True
        else:
            employee['active'] = False 
        return employee

     
    def lookup_level(self, employee):
        'Find the corresponding leave grant level in the table'

        try:
            return Leave_Grant_Level.objects.get(level=employee['level'])
        except Exception as e:
            print(f'Unable to find {employee["level"]} in the level table for {employee["first_name"]} {employee["last_name"]} - {e}')


    def add_to_db(self,employee):
        'Add dictionary input to the beneficiary and leave grant models'
        try:
            beneficiary = Beneficiary.objects.create(
                                                    employee_id = employee['employee_id'],
                                                    first_name  = employee['first_name'],
                                                    last_name  = employee['last_name'],
                                                    eligibility_date = employee['eligibility_date'],
                                                    active = employee['active'],                                                     
                                                    leave_grant_level = self.lookup_level(employee),
                                                    job_title  = employee['job_title'],
            )
            print(f'Adding employee... {employee["first_name"]} {employee["last_name"]}\n')      
            
            for payment in employee['payments']:                
                if payment['date'] == '':
                     payment['date'] = None

                if payment['amount'] != '': 
                    try:                     
                        Leave_Grant_Detail.objects.create(
                                                        beneficiary =  beneficiary, 
                                                        payment_date = payment['date'],
                                                        amount = payment['amount'],
                                                        remarks = payment['who']
                        )
                    except Exception as e:
                        print(f'Payment details not successful {e} - {beneficiary}: {payment["amount"]}')

         
        

        except:
            # print(f'there was an error {employee}')
            self.count += 1
            self.error_records.append(f'{employee["first_name"]} {employee["last_name"]}')

        
    def add_levels(self):
        'Add leave grant levels and percentages to the table'
        levels = [  ('J1-J3',30), 
                    ('J4-J6',40),
                    ('J7-J9',60),
                    ('S1-S4',80),
                    ('S5-S8',100)
                    ]

        for level in levels:
            try:
                Leave_Grant_Level.objects.create(level=level[0], leave_grant_percentage=level[1])
                print(f'Adding...{level}')
            except Exception as e:
                print(f'Could not add level {level} - {e} ')   
       


    def handle(self, *args, **kwargs):
        file_name = kwargs['file_name']
        first_run = True       
        self_payments = []             
        employee = {}
        
        # Add the grant levels first
        # self.add_levels()
        Leave_Grant_Detail.objects.all().delete()
        Beneficiary.objects.all().delete()
        Dependent.objects.all().delete()
       
        with open(file_name) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            
            for row in csv_reader:
                if not first_run and row[0] != '':                    
                    self.add_to_db(employee)                                
                    self_payments = []
                         
                # Check if employee id exists, if yes print self value               
                if row[0] != '':                     
                    employee = self.add_bio(row[0],row[1],row[2],row[4],row[3],row[9],row[10])            
                    self_payments.append(self.add_payment(row[5],row[6],'Self Payment'))                 
                    
                    
                    if (row[7] != '') and row[7] != '0':    # check if dependents cell is empty or zero
                        self_payments.append(self.add_payment(row[5],row[7],'Dependent Payment'))
                       
               
                if row[0] == '':
                    if row[6] != '' and row[6] != '0':
                        self_payments.append(self.add_payment(row[5],row[6],'Self Payment'))                       

                    if row[7] != '' and row[7] != '0':
                        self_payments.append(self.add_payment(row[5],row[7], 'Dependent Payment'))                     

                
                first_run = False
                employee['payments'] = self_payments 
            
            self.add_to_db(employee)    # add the last employee
           
            for record in self.error_records:
                print(record)
            print(self.count)
        
            
   





