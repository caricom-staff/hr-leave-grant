from django.core.management.base import BaseCommand
from hr_leave_grant_app.models import Dependent, Beneficiary, Leave_Grant_Detail, Leave_Grant_Year_Value
import datetime

import csv

class Command(BaseCommand):
    help = 'Searches for leave grant anniversaries'      

    def find_age(self,dob):
        'Returns the age of the dependent'
        return  int(((datetime.date.today() - dob).days) / 365.25)

    def get_leave_grant_value(self,year):
        'get the leave grant value for the current year'
        try:
            leave_grant_value = Leave_Grant_Year_Value.objects.get(year__year=year)
        except:
            print(f'Leave Grant value not available')
        return leave_grant_value.value


    def create_leave_grant_record(self,beneficiary,current_value,percentage,remarks,year):
        'insert a record into the leave_grant_details table'
        try:
            Leave_Grant_Detail.objects.create(beneficiary=beneficiary,
                                                amount= float(current_value) * float(percentage/100),
                                                remarks=remarks,
                                                transaction_year=datetime.date.today()
                                            )
            print(f'Created leave grant record for - {remarks}')
        except Exception as e:
            print(f'Unable to create leave grant record for -{current_value * percentage/100} {remarks} {e}')


    def handle(self, *args, **kwargs):

        adult_dependents = ('Wife', 'Husband', 'Mother', 'Father')
        month = datetime.date.today().month
        year = datetime.date.today().year
        current_value = self.get_leave_grant_value(year)
        
       
        try:
            active_beneficiaries = Beneficiary.objects.filter(active=True).filter(eligibility_date__month=month)
        except Exception as e:
            print(e)
        
                
   
        for beneficiary in active_beneficiaries:
            #Check to ensure this beneficiary doesn't already have records for the current year
            if not(Leave_Grant_Detail.objects.filter(beneficiary=beneficiary, transaction_year__year=year)):
                beneficiary_remarks = f'{beneficiary} for period {year-1} - {year}'
                percentage = beneficiary.leave_grant_level.leave_grant_percentage
                
                self.create_leave_grant_record(beneficiary,current_value,percentage,beneficiary_remarks,year)
                
                active_dependents = Dependent.objects.filter(beneficiary=beneficiary, active=True)
                
                for dependent in active_dependents[0:3]:
                    dependent_age = self.find_age(dependent.date_of_birth)
                    dependent_remarks = f'{beneficiary} ({dependent.dependent_type} - {dependent}) - for period {year-1} - {year}'


                    if dependent.dependent_type in adult_dependents:                        
                        self.create_leave_grant_record(beneficiary,current_value,percentage,dependent_remarks,year)                   

                    elif dependent_age < 12:                        
                        self.create_leave_grant_record(beneficiary,current_value/2,percentage,dependent_remarks,year)
                        
                    elif (dependent_age >=12) and (dependent_age < 18):                       
                        self.create_leave_grant_record(beneficiary,current_value,percentage,dependent_remarks,year)
                        
                    elif (dependent_age >=18) and (dependent_age < 23):
                        
                        if (dependent.active) and (dependent.further_education):
                            self.create_leave_grant_record(beneficiary,current_value,percentage,dependent_remarks,year)

                
     
            
