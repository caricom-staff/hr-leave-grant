from django.contrib import admin
from hr_leave_grant_app.models import (Beneficiary, 
                                        Dependent, 
                                        Leave_Grant_Detail, 
                                        Leave_Grant_Level,
                                        Leave_Grant_Year_Value)

# admin.site.register(Beneficiary)
admin.site.register(Dependent)
admin.site.register(Leave_Grant_Level)
# admin.site.register(Leave_Grant_Detail)

@admin.register(Leave_Grant_Year_Value)
class Leave_grant_Year_ValueAdmin(admin.ModelAdmin):
    list_display = ('year', 'value')


class Leave_Grant_DetailInline(admin.TabularInline):
    model = Leave_Grant_Detail
    extra = 0

class DependentInline(admin.TabularInline):
    model = Dependent
    extra = 0

@admin.register(Beneficiary)
class BeneficiaryAdmin(admin.ModelAdmin):
    list_display = ('employee_name','active','leave_grant_level')
    list_filter = ('leave_grant_level','active')
    search_fields = ('first_name','last_name')
    inlines = [DependentInline,Leave_Grant_DetailInline]
    
    def employee_name(self, instance):
        return f'{instance.first_name} {instance.last_name} '



class Leave_Grant_LevelAdmin(admin.ModelAdmin):
    list_display = ('employee_name', 'payment_date','amount')

    def employee_name(self, instance):
        return f'{instance.beneficiary.first_name} {instance.beneficiary.last_name} '

admin.site.register(Leave_Grant_Detail,Leave_Grant_LevelAdmin)


