from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.db.models import Sum

from django.views import View
from django.views.generic import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse,reverse_lazy
from rest_framework import generics
from rest_framework.filters import SearchFilter

from hr_leave_grant_app.pagination import SmallSetPagination

from hr_leave_grant_app.serializers import (BeneficiarySerializer,
                                            BeneficiaryDetailSerializer,
                                            DependentSerializer,
                                            LeaveGrantDetailSerializer,
                                            LeaveGrantLevelSerializer
                                            )
from hr_leave_grant_app.models import   (Beneficiary,
                                        Dependent,
                                        Leave_Grant_Detail,
                                        Leave_Grant_Level
                                        )
import datetime

                                        
class BeneficiaryListCreateAPIView(generics.ListCreateAPIView):
    queryset = Beneficiary.objects.all()
    serializer_class = BeneficiarySerializer
    filter_backends = [SearchFilter]
    search_fields = ["first_name", "last_name", "employee_id"]
    pagination_class = SmallSetPagination


class BeneficiaryDetailAPiView(generics.RetrieveUpdateDestroyAPIView): #might need to change this to listAPI
    queryset = Beneficiary.objects.all()
    serializer_class = BeneficiaryDetailSerializer
    search_fields = ["id"]

class DependentCreateAPIView(generics.CreateAPIView):
    'View to create dependents'
    queryset = Dependent.objects.all()
    serializer_class = DependentSerializer

    def perform_create(self, serializer):
        beneficiary_pk = self.kwargs.get('beneficiary_pk')
        beneficiary = get_object_or_404(Beneficiary, pk=beneficiary_pk)
        serializer.save(beneficiary=beneficiary)

class DependentDetailAPIView(LoginRequiredMixin,generics.RetrieveUpdateDestroyAPIView):
    'View to list individual dependent details'
    queryset = Dependent.objects.all()
    serializer_class = DependentSerializer


class BeneficiaryListView(LoginRequiredMixin,ListView):
    'Generic View to list Beneficiaries'
    model = Beneficiary
    paginate_by = 10

    def get_queryset(self):
        'Search for the name in the search form'
        queryset = super().get_queryset()
        search = self.request.GET.get('search')        
        if search:
            results = self.model.objects.filter(Q(first_name__icontains = search) | Q(last_name__icontains = search))
        else:
            results = self.model.objects.all()
        return results


class BeneficiaryPaymentsDetailView(LoginRequiredMixin,DetailView):
    'View to return all payments for the Beneficiary'
    model = Beneficiary
    template_name = 'hr_leave_grant_app/beneficiary_payment.html'

    def get_context_data(self, **kwargs):
        context = super(BeneficiaryPaymentsDetailView, self).get_context_data(**kwargs)
        outstanding = Leave_Grant_Detail.objects.filter(beneficiary=self.kwargs['pk']).exclude(payment_date__isnull=False)
        context['outstanding_payments'] = outstanding
        total = outstanding.aggregate(outstanding_total=Sum('amount'))
        context['outstanding_total'] = total
        # context['outstanding_total'] = outstanding.aggregate(outstanding_total=Sum('amount'))
        context['payments'] = Leave_Grant_Detail.objects.filter(beneficiary=self.kwargs['pk']).exclude(payment_date__isnull=True).order_by('-payment_date')        
        self.request.session['outstanding_total'] = str(total)        
        return context
        

class CreateBeneficiaryView(LoginRequiredMixin,CreateView):
    'View to create beneficiaries'
    model = Beneficiary
    template_name = 'hr_leave_grant_app/beneficiary_form.html'
    fields = '__all__'

    def get_success_url(self):
        return reverse('beneficiary-detail', kwargs={'pk': self.object.id})

class UpdateBeneficiaryView(LoginRequiredMixin,UpdateView):
    model = Beneficiary    
    template_name = 'hr_leave_grant_app/beneficiary_form.html'
    fields = '__all__'
    
    def get_context_data(self, **kwargs):
        'Send the Update text to the form'
        context = super(UpdateBeneficiaryView, self).get_context_data(**kwargs)
        context['form_type'] = 'update'
        return context


class CreateDependentView(LoginRequiredMixin,CreateView):
    'View to create dependents'
    model = Dependent
    template_name = 'hr_leave_grant_app/dependent_form.html'
    fields = ['first_name', 'last_name', 'dependent_type', 'date_of_birth', 'active', 'further_education']
    

    def form_valid(self, form):
        try:
            beneficiary = get_object_or_404(Beneficiary,pk=self.kwargs['pk'])
            
        except Exception as e:
            return e
        form.instance.beneficiary = beneficiary
        return super(CreateDependentView, self).form_valid(form)

    def get_success_url(self):
        return reverse('beneficiary-detail', kwargs={'pk': self.kwargs['pk']})


class UpdateDependentView(LoginRequiredMixin,UpdateView):
    'View to update dependents'
    model = Dependent
    template_name = 'hr_leave_grant_app/dependent_form.html'
    fields = ['first_name', 'last_name', 'dependent_type', 'date_of_birth', 'active','further_education']
    
    def get_context_data(self, **kwargs):
        context = super(UpdateDependentView,self).get_context_data(**kwargs)
        context["form_type"] = 'update' 
        return context   
    

    def get_success_url(self):
        return reverse('beneficiary-detail', kwargs={'pk': self.request.session['beneficiary_id']})




class ConfirmPaymentView(LoginRequiredMixin,View):
    'Confirms and processes the payment authorisation request'
    template_name = 'hr_leave_grant_app/confirm_payment.html'

    def get(self, request,pk):
        'Display payment authorisation confirmation page'
        beneficiary_id = self.kwargs['pk'] #self.request.session['beneficiary_id']    
        outstanding_total = self.request.session['outstanding_total']

        try: 
            bene = Beneficiary.objects.get(id=self.kwargs['pk'])
        except Exception as e:
            print(e)
            
        return render(request, self.template_name, {'outstanding_total': outstanding_total,
                                                    'beneficiary_id': beneficiary_id,  
                                                    'bene': bene ,                                                 
                                                    })

    def post(self, request,pk):
        'Update the leave grant details table and redirect to payment details page'
        
        user = f'{request.user.first_name} {request.user.last_name}'
        try:
            updated_records = Leave_Grant_Detail.objects.filter(beneficiary=self.kwargs['pk'])\
                                                        .exclude(payment_date__isnull=False)\
                                                        .update(payment_date=datetime.date.today(),recorded_by=user)
                                                        
        except Exception as e:
            print(e)

        return redirect('beneficiary-payment',self.kwargs['pk'])
        

class PendingPaymentView(LoginRequiredMixin,ListView):
    'Return a list of Employees with pending payments'
    context_object = 'beneficiary'  
    paginate_by = 10  
    template_name = 'hr_leave_grant_app/pending_payment.html'
    queryset = Beneficiary.objects.filter(leave_grant_details__payment_date__isnull=True, leave_grant_details__amount__isnull=False).distinct()

    
class BeneficiaryDetailView(LoginRequiredMixin,DetailView):
    'View to return all details for the Beneficiary including dependents'
    model = Beneficiary
    template_name = 'hr_leave_grant_app/beneficiary_detail.html'

    def get_context_data(self, **kwargs):
        context = super(BeneficiaryDetailView, self).get_context_data(**kwargs)       
        self.request.session['beneficiary_id'] = self.kwargs['pk']        
        return context

    
class DependentListView(LoginRequiredMixin,ListView):
    'Generic View to list All Dependents'
    model = Dependent
    paginate_by = 10
    template_name = 'hr_leave_grant_app/dependent_all.html'


class DependentOver18ListView(LoginRequiredMixin,ListView):
    'Generic View to list All child Dependents over 18 years old'
    model = Dependent
    paginate_by = 10
    template_name = 'hr_leave_grant_app/dependent_over18.html'    
    
    def get_queryset(self):
        'Search for dependents > 18'
        dependents = ['Son', 'Daughter']
        queryset = super().get_queryset()
        
        queryset = Dependent.objects.filter(dependent_type__in=dependents,active=True)
        return queryset


class TransactionHistoryView(LoginRequiredMixin,ListView):
    'View to list all records in the leave grant detail table'
    model = Leave_Grant_Detail
    paginate_by = 15
    template_name = 'hr_leave_grant_app/transaction_history.html'
    ordering = ['-transaction_year']