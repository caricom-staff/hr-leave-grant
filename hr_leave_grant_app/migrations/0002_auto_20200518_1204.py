# Generated by Django 3.0.6 on 2020-05-18 12:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hr_leave_grant_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='leave_grant_detail',
            options={'ordering': ['beneficiary']},
        ),
    ]
