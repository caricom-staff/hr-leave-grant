# Generated by Django 3.0.6 on 2020-05-31 13:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hr_leave_grant_app', '0015_auto_20200527_0237'),
    ]

    operations = [
        migrations.AddField(
            model_name='dependent',
            name='further_education',
            field=models.BooleanField(default=False),
        ),
    ]
