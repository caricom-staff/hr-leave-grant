# Generated by Django 3.0.6 on 2020-05-26 18:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hr_leave_grant_app', '0010_auto_20200526_1758'),
    ]

    operations = [
        migrations.RenameField(
            model_name='leave_grant_detail',
            old_name='year',
            new_name='transaction_year',
        ),
    ]
