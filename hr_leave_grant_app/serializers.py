from rest_framework import serializers



from hr_leave_grant_app.models import ( Beneficiary,
                                        Dependent,
                                        Leave_Grant_Detail,
                                        Leave_Grant_Level
                                        )
                                        

class BeneficiarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Beneficiary
        exclude = ('updated_at', 'created_at',)


class DependentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dependent
        # fields = '__all__'
        exclude = ('beneficiary','updated_at', 'created_at',)

class LeaveGrantDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Leave_Grant_Detail
        exclude = ('updated_at', 'created_at',)

class LeaveGrantLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Leave_Grant_Level
        fields = ('level',)

class BeneficiaryDetailSerializer(serializers.ModelSerializer):
    'Returns all related fields for the Beneficiary Model (Dependents and Details'
    
    dependent = DependentSerializer(many=True, read_only=True)    
    leave_grant_details = LeaveGrantDetailSerializer(many=True, read_only=True)
    leave_grant_level = LeaveGrantLevelSerializer(read_only=True)
    card = serializers.FileField(read_only=True)

    class Meta:
        model = Beneficiary
        exclude = ('updated_at', 'created_at')

  